/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['media.guim.co.uk']
  }
}

module.exports = nextConfig
