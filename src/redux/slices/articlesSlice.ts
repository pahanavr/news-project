import { getArticles } from "@/lib/api/api";
import { Article, OrderBy, RequestType } from "@/lib/types";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

export interface ArticlesState {
  articlesList: Article[]
  currentArticleId: string
  status: 'idle' | 'loading' | 'succeeded' | 'failed'
  error: string | null
  page: number
  searchValue: string,
  isFetching: boolean,
  pageSize: number,
  orderBy: OrderBy
}

export const initialState: ArticlesState = {
  articlesList: [],
  currentArticleId: '',
  status: 'idle',
  error: null,
  page: 1,
  searchValue: '',
  isFetching: false,
  pageSize: 20,
  orderBy: 'newest'
}

export const fetchArticles = createAsyncThunk(
  'articles/fetchArticles',
  async (request: RequestType, { rejectWithValue }) => {
    try {
      const articles = await getArticles(request);
      return articles || [];
    } catch (error: any) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const articlesSlice = createSlice({
  name: 'articles',
  initialState,
  reducers: {
    setArticles: (state, action: PayloadAction<Article[]>) => {
      state.articlesList = action.payload;
    },
    setCurrentArticleId: (state, action: PayloadAction<string>) => {
      state.currentArticleId = action.payload;
    },
    resetCurrentArticleId: (state) => {
      state.currentArticleId = ''
    },
    setNextPage: (state, action: PayloadAction<number>) => {
      state.page = action.payload
    },
    setSearchValue: (state, action: PayloadAction<string>) => {
      state.searchValue = action.payload
    },
    setIsFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload
    },
    setCurrentPageSize: (state, action: PayloadAction<number>) => {
      state.pageSize = action.payload
    },
    setOrderBy: (state, action: PayloadAction<OrderBy>) => {
      state.orderBy = action.payload
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchArticles.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchArticles.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message ?? 'Failed to fetch articles';
      })
      .addCase(fetchArticles.fulfilled, (state, action: PayloadAction<Article[]>) => {
        state.status = 'succeeded';
        if (state.page > 1) {
          state.articlesList = [...state.articlesList, ...action.payload];
        } else {
          state.articlesList = action.payload;
        }
      });
  }
})

export const {
  setArticles,
  setCurrentArticleId,
  resetCurrentArticleId,
  setNextPage,
  setSearchValue,
  setIsFetching,
  setCurrentPageSize,
  setOrderBy
} = articlesSlice.actions

export const selectArticlesList = (state: RootState) => state.articles.articlesList
export const selectCurrentArticlesId = (state: RootState) => state.articles.currentArticleId
export const selectCurrentArticleById = (id: string) => (state: RootState) => {
  return state.articles.articlesList.find(article => article.id === id);
};
export const selectCurrentPage = (state: RootState) => state.articles.page;
export const selectSearchValue = (state: RootState) => state.articles.searchValue;
export const selectIsFetching = (state: RootState) => state.articles.isFetching;
export const selectPageSizeValue = (state: RootState) => state.articles.pageSize;
export const selectOrderByValue = (state: RootState) => state.articles.orderBy;

export default articlesSlice.reducer
