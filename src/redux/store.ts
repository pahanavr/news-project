import { Action, configureStore } from '@reduxjs/toolkit';
import articlesSlice from './slices/articlesSlice';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

export const store = configureStore({
  reducer: {
    articles: articlesSlice
  },
  devTools: true
})

export type AppDispatch = ThunkDispatch<RootState, unknown, Action<string>>;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
