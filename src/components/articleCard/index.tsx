import { formatDate } from '@/lib/utils'
import styles from './styles.module.css'
import Image from 'next/image'
import Link from 'next/link'
import iconRight from '../../../public/images/icon-right.svg'
import image404 from '../../../public/images/image-404.png'
import { useAppDispatch } from '@/redux/hooks'
import { setCurrentArticleId } from '@/redux/slices/articlesSlice'

interface ArticleCardProps {
  id: string
  thumbnail: string
  webPublicationDate: Date
  webTitle: string
}

export const ArticleCard = ({
  id,
  thumbnail,
  webPublicationDate,
  webTitle,
}: ArticleCardProps) => {
  const dispatch = useAppDispatch()

  return (
    <article className={styles.article}>
      <div className={styles.article__imageBlock}>
      <Image
        className={styles.article__image}
        src={thumbnail ? thumbnail : image404}
        fill={true}
        alt={webTitle}
      />
      </div>
      <div className={styles.article__info}>
        <p className={styles.article__date}>{formatDate(webPublicationDate)}</p>
        <h3 className={styles.article__title}>{webTitle}</h3>
        <Link 
          className={styles.article__button}
          href={`/${id}`}
          onClick={() => dispatch(setCurrentArticleId(id))}
        >
          <span className={styles.article__buttonText}>Details</span>
          <Image width={16} height={16} src={iconRight} alt={'icon-right'} />
        </Link>
      </div>
    </article>
  )
}