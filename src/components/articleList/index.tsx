import { Article } from '@/lib/types'
import styles from './styles.module.css'
import { ArticleCard } from '../articleCard'

export const ArticleList = ({articles}: {articles: Article[]}) => {
  return (
    <section className={styles.articles}>
      {articles.map(article => (
        <ArticleCard 
          key={article.id}
          id={article.id} 
          thumbnail={article.fields?.thumbnail} 
          webPublicationDate={article.webPublicationDate} 
          webTitle={article.webTitle} 
        />
      ))}
    </section>
  )
}
