import { OrderBy } from '@/lib/types'
import styles from './styles.module.css'

interface SearchBarProps {
  onSearch: () => void
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  value: string
  onChangePageSize: (e: React.ChangeEvent<HTMLSelectElement>) => void
  pageSizeValue: number
  onChangeOrderBy: (e: React.ChangeEvent<HTMLSelectElement>) => void
  orderByValue: OrderBy
}

export const SearchBar = ({
  onSearch,
  onChange,
  value,
  onChangePageSize,
  pageSizeValue,
  onChangeOrderBy,
  orderByValue
}: SearchBarProps) => {
  const pageSizes = [20, 50, 100]
  const orderByVariants = ['newest', 'relevance']

  return (
    <div className={styles.searchBar}>
      <div className={styles.searchBar__searchBlock}>
        <input
          className={styles.searchBar__searchInput}
          type='search'
          placeholder='Search'
          value={value}
          onChange={onChange}
        />
        <button 
          className={styles.searchBar__button}
          onClick={onSearch}
        >
          Find
        </button>
      </div>
      <div className={styles.searchBar__sortBlock}>
        <select 
          className={styles.searchBar__select}
          onChange={onChangeOrderBy}
          value={orderByValue}
        >
          {orderByVariants.map((variant, index) => (<option key={index}>{variant}</option>))}
        </select>
        <div className={styles.searchBar__selectBlock}>
          <span className={styles.searchBar__pagesTitle}>Items on page:</span>
          <select
            className={styles.searchBar__select}
            onChange={onChangePageSize}
            value={pageSizeValue}
          >
            {pageSizes.map((pageSize, index) => (<option key={index}>{pageSize}</option>))}
          </select>
        </div>
      </div>
    </div>
  )
}
