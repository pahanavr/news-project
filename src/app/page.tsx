'use client'

import { useEffect } from 'react'
import { ArticleList } from '../components/articleList'
import styles from './page.module.css'
import { useAppDispatch, useAppSelector } from '@/redux/hooks'
import { fetchArticles, resetCurrentArticleId, selectArticlesList, selectCurrentPage, selectIsFetching, selectOrderByValue, selectPageSizeValue, selectSearchValue, setCurrentPageSize, setIsFetching, setNextPage, setOrderBy, setSearchValue } from '@/redux/slices/articlesSlice'
import { SearchBar } from '@/components/searchBar'
import { OrderBy } from '@/lib/types'

export default function MainPage() {
  const dispatch = useAppDispatch()
  const articles = useAppSelector(selectArticlesList)
  const page = useAppSelector(selectCurrentPage)
  const searchValue = useAppSelector(selectSearchValue)
  const isFetching = useAppSelector(selectIsFetching)
  const pageSizeValue = useAppSelector(selectPageSizeValue)
  const orderByValue = useAppSelector(selectOrderByValue)

  const handleSearch = () => {
    dispatch(fetchArticles({q: searchValue, "page-size": pageSizeValue, "order-by": orderByValue}))
  }

  const handleLoadMore = () => {
    dispatch(setIsFetching(true));
    dispatch(setNextPage(page + 1))
    dispatch(fetchArticles({ page: page + 1, "page-size": pageSizeValue, "order-by": orderByValue }))
      .then(() => dispatch(setIsFetching(false)))
      .catch(() => dispatch(setIsFetching(false)));
  };

  const handleChangePageSize = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const newValue = Number(e.target.value)
    dispatch(setCurrentPageSize(newValue))
    dispatch(fetchArticles({ q: searchValue, 'page-size': newValue, "order-by": orderByValue }))
  }

  const handleChangeOrderBy = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const newValue = e.target.value as OrderBy
    dispatch(setOrderBy(newValue))
    dispatch(fetchArticles({ q: searchValue, "page-size": pageSizeValue, 'order-by': newValue }))
  }

  useEffect(() => {
    const fetchData = async () => {
      setIsFetching(true);
      try {
        await dispatch(fetchArticles({}));
        dispatch(resetCurrentArticleId());
      } catch (error) {
        console.error("Error fetching articles:", error);
      } finally {
        setIsFetching(false);
      }
    };

    fetchData();
  }, [dispatch]);

  return (
    <main className={styles.main}>
      <SearchBar 
        value={searchValue} 
        onSearch={handleSearch} 
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => dispatch(setSearchValue(e.target.value))}
        onChangePageSize={handleChangePageSize}
        pageSizeValue={pageSizeValue}
        onChangeOrderBy={handleChangeOrderBy}
        orderByValue={orderByValue}
      />
      {articles.length === 0 ?
        'Nothing found'
        : <ArticleList articles={articles} />
      }
      {articles.length !== 0 &&
      <button className={styles.main__buttonMore} onClick={handleLoadMore} disabled={isFetching}>
        {isFetching ? 'Loading...' : 'Load More'}
      </button>}
    </main>
  )
}