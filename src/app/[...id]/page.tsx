import { getArticle, getArticles } from "@/lib/api/api"
import { Article } from "@/lib/types";
import Link from "next/link";
import styles from './page.module.css'
import { formatDate } from "@/lib/utils";
import Image from 'next/image'
import image404 from '../../../public/images/image-404.png'

export async function generateStaticParams() {
  const articles: Article[] = await getArticles({})

  return articles.map(article => ({
    params: { id: article.id.split("/") }
  }))
}

export default async function ArticlePage({ params: {id}}: {params: {id: string[]}}) {
  const article = await getArticle(id.join("/"))

  return (
    <div className={styles.articlePage}>
      <Link className={styles.articlePage__backButton} href='/'>Go to main page</Link>
      <h1 className={styles.articlePage__title}>{article?.webTitle}</h1>
      <div className={styles.articlePage__info}>
        <span>{formatDate(article?.webPublicationDate || new Date())}</span>
        <a
          target="_blank"
          href={article?.webUrl}
          className={styles.articlePage__link}
        >
          read on Guardian
        </a>
      </div>
      <div className={styles.articlePage__main}>
        <Image
          className={styles.articlePage__image}
          src={article?.fields.thumbnail || image404}
          width={500}
          height={300}
          alt={article?.webTitle || 'image-404'}
        />
        <p className={styles.articlePage__text}>
          {article?.fields.bodyText}
        </p>
      </div>
    </div>
  )
}
