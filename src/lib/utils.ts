import dayjs from 'dayjs'
import { isEmpty, isNil, omitBy } from 'lodash'

export const buildQuery = (params: any) => {
  const normalizedParams = omitBy(params, isNil)

  if (isEmpty(normalizedParams)) {
    return ''
  }

  const query = new URLSearchParams({
    ...normalizedParams
  })

  return `?${query}`
}

export const formatDate = (date: Date): string => {
  return dayjs(date).format('D MMMM YYYY, hh:mm:ss A')
}
