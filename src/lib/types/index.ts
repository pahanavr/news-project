export interface Article {
  id: string
  type: string
  sectionId: string
  sectionName: string
  webPublicationDate: Date
  webTitle: string
  webUrl: string
  apiUrl: string
  fields: {
    thumbnail: string
    bodyText: string
  }
  isHosted: boolean
  pillarId: string
  pillarName: string
}

export type OrderBy = 'relevance' | 'newest'

export interface RequestType {
  q?: string
  page?: number
  'page-size'?: number
  'order-by'?: OrderBy
}
