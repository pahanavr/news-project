import { API_BASE_URL } from '../constants'
import { buildQuery } from '../utils'

export async function fetchAPI(
  path: string,
  params: Record<string, any> = {},
): Promise<any> {
  const query = buildQuery({
    ...params,
    'api-key': '4e45f54e-8571-472b-9528-e209165cb9c6'
  })

  const url = `${API_BASE_URL}${path}${query}`
  const response = await fetch(url)
  const data = await response.json()

  return data
}
