import { fetchAPI } from '.'
import { Article, RequestType } from '../types'

export async function getArticles(request: RequestType): Promise<Article[]> {
  const response = await fetchAPI('search', {
    'page-size': request['page-size'] || 20,
    'show-fields': 'thumbnail',
    q: request.q,
    page: request.page,
    'order-by': request['order-by'] || null
  })
  const articles = response.response.results

  return articles
}

export async function getArticle(id: string): Promise<Article> {
  const response = await fetchAPI(`${id}`, {
    'show-fields': 'thumbnail,bodyText'
  })
  const article = response.response.content

  return article
}
